# Bowling Game

Pour ce TP, vous allez faire du TDD seul.

Il existe un très grand nombre d'implémentations de cet exercice sur internet, vous aider de l'une d'entre elle est probablement la pire des solutions pour apprendre quelque chose aujourd'hui.

    1 4 | 4 5 | 6 / | 5 / |   X | 0 1 | 7 / | 6 / |   X | 2 / 6 |
    5   | 14  | 29  | 49  | 60  | 61  | 77  | 97  | 117 | 133   |

Une partie de bowling compte dix carreaux (ou 10 frames). Chaque joueur lance deux boules à chaque carreau, sauf en cas d'abat (strike). Un abat consiste à faire tomber les dix quilles avec la première boule. La réserve (spare) consiste à faire tomber les dix quilles avec les deux tirs consécutifs du carreau.

* En cas d'abat, indiqué par un « X » : 10 + nombre de quilles abattues après les deux lancers suivants.
* En cas de « réserve », indiquée par le nombre de quilles renversées au premier lancer, suivi d’un « / », par exemple « 8 / ») : 10 + nombre de quilles abattues au lancer suivant.
* Trou (ou jeu ouvert) : nombre de quilles abattues.
* Le dixième jeu est particulier : en cas d'abat au premier lancer, deux lancers supplémentaires sont accordés. En cas de réalisation d’une réserve, un lancer supplémentaire est accordé.

Ainsi, la marque parfaite est de 300 points, pour douze abats consécutifs.

## Rappel des règles de clean code

- pas de méthode de plus de 10 lignes
- nommage correct des variables et des méthodes : on comprend à quoi elles servent à la lecture et il n'y a pas d'abréviation.
- pas plus de 3 indentations par méthode
- pas de classe de plus de 50 lignes
- pas de commentaire, on comprend le code à la première lecture
- pas de répétition (DRY)
- le code est le plus simple possible : si je peux enlever du code sans casser les tests c'est que je n'ai pas fini ma simplification
- Pas de "magic numbers", des nombre ou des Strings qui sortent de nulle part.
- Max 2 paramètres par méthode
- Pas plus d'un point par ligne (cf. notation fluent)

## Initialisation

### configuration de maven

* s'il n'existe pas déjà créez un dossier ".m2" : `mkdir $HOME/.m2`
* création du fichier de configuration : `touch $HOME/.m2/settings.xml`
* avec votre éditeur de texte préféré, mettez ce qu'il y a ci-dessous dans `settings.xml`
* finalement, vérifiez votre installation : `mvn -version`

```xml
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0
                          http://maven.apache.org/xsd/settings-1.0.0.xsd">
    <proxies>
      <proxy>
        <id>proxy</id>
        <active>true</active>
        <protocol>http</protocol>
        <host>cache.univ-lille1.fr</host>
        <port>3128</port>
      </proxy>
    </proxies>
</settings>
```

### Projet

* Si ce n'est pas déjà fait, demandez à rejoindre le groupe gitlab suivant : https://gitlab.univ-lille.fr/2019-coa-tdd/groupe-[Votre groupe]/
* Créez un projet dans ce groupe, nommé `tp2-nom`
* Dans un terminal, si ce n'est pas déjà fait, créez un workspace pour la coa-tdd `mkdir ~/ws-coa-tdd`
* Clonez le squelette de projet maven `git clone https://gitlab.univ-lille.fr/2019-coa-tdd/maven-skeleton.git ~/ws-coa-tdd/tp2-nom`
* Dans un terminal, entrez dans le répertoire `~/ws-coa-tdd/tp2-nom`
* Changer l'URL du projet gitlab pour y mettre la votre `git remote set-url origin URL`
* Modifier le fichier `pom.xml` changez le nom du projet dans le fichier pom.xml `sed -i -e 's/maven-skeleton/tp2-nom/g' pom.xml`
* Lancez les tests pour vérifier que tout fonctionne `mvn test`
* Ouvrez éclipse `eclipse -data ~/ws-coa-tdd/`
* Importez votre nouveau projet maven à l'aide du menu File > Import.

## Première étape

Écrire une classe BowlingGame avec 2 méthodes :
- roll(pins : int) qui est appelé à chaque lancé. L'argument est le nombre de quilles abattues durant le lancé.
- score() : int qui retourne le nombre de points courant de la partie.

``` java
public class BowlingGame {
    public void roll (int pins) {
        ...
    }

    public int score () {
        ...
    }
}
```

Tous les tests d'acceptances prendront en entrée la réprésentation ASCII de la partie. Nous pourrions avoir par exemple :

``` java
public class BowlingGameTest {
    @Test
    public void score_should_return_the_sum_of_knocked_pins () {
        BowlingGame game = playOnNewGame("7 2 | 4 4 | 1 3 |");
        assertEquals(21, game.score());
    }

    @Test
    @Ignore
    public void score_should_take_the_spare_bonus () {
        BowlingGame game = playOnNewGame("6 / | 4 ");
        assertEquals(18, game.score());
    }

    ...

}
```

## Seconde étape

Ajouter la méthode toString() qui retourne la représentation ASCII de la partie en cours comme ci dessus.

``` java
public class BowlingGame {
    public String toString () {
        ...
    }
}
```

